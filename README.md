# Superpowers Game Text Editor Theme - Vs Dark


An 'easy on the eyes' dark text editor theme for [Superpowers, the extensible HTML5 2D+3D game engine](http://superpowers-html5.com).

###
![theme preview](http://i.imgur.com/4qil7HN.jpg)
###

##Installation
Copy the `widget.css` file into the `app/systems/game/plugins/common/textEditorWidget/public` folder and replace the file already there.
(***NOTE:*** It's recommended to ***back up your previous widget.css file*** before overwriting)

## Credits:
- [StuffBySpencer](https://github.com/StuffBySpencer/) for commented widget.css file to work from.